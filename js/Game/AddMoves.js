/**
 * Add moves to a board passed by reference
 */

 // TODO : refactor this module to a more functional approach
class AddMoves{
    constructor(){
        this._boardRef = null;
    }

    /**
     * Add move to a 1D array, if move is added return true, else return false
     * @param {1D array} board 
     * @param {number} index (0-6)
     */
    addMove(board, index, fillValue){
        //set boardRef to the correct board reference and cancel old reference
        this._redirectUpdateBoardRef(board);

        //check if move is valid
        if(this._moveNotValid(index)){
            return false;
        }

        //check which row move fills
        let row = this._whichRow(index);

        //fill move
        this._fillMove(row, fillValue);

        return true;
    }

    /**
     * Checks if move is valid otherwise return false
     * looks by checking if array values 1 through 6 are empty/set to zero
     */
    _moveNotValid(index){
        // if column index is not zero move is not valid return true
        if(this._boardRef[index] !== 0){
            return true
        }
        // move is not valid
        return false
    }

    /**
     * find the correct index of column chosen, it's a 1D array
     * 
     * offset: ex if column 0 is chosen, first index to be checked 35 then 28 then 21
     * @param {number} index 
     */
    _whichRow(index) {
        let offset = 6 - index;
        for (let i = 0; i < 6; i++) {
            let currentIndexChecked = 41 - ((i * 7) + offset)
            if (this._boardRef[currentIndexChecked] === 0) {
                return currentIndexChecked;
            }
        }
    }

    /**
     * fill the current board (this.board)
     * @param {number} index 
     * @param {number} player (1 or 2)
     */
    _fillMove(row, player) {
        this._boardRef[row] = player;
    }

    /**
     * Assign the board reference to this.board, modification for this that board will change the passed board reference.
     * @param {1D array} board 
     */
    _redirectUpdateBoardRef(board){
        this._boardRef = board;
    }
}
export default new AddMoves()