function isWinner(board) {
    if (_optimizeHorizontalChecks(board)) {
        return true;
    }
    if (_optimizeVerticalChecks(board)) {
        return true;
    }

    return false
}

function _optimizeHorizontalChecks(board) {
    const checks = [38, 31, 24, 17, 10, 3]
    for (let i = 0; i < checks.length; i++) {
        if (board[checks[i]] === 0) {
            return false
        }
        // -3 offset from center column to first column
        if (_checkHorizontal(checks[i] - 3, board)) {
            return true
        }
    }
    return false
}
function _optimizeVerticalChecks(board) {

    // horizontal, back diagonal, forward diagonal
    const startIndexes = [
        [0, 14, null],
        [1, 7, 3],
        [2, 0, 4],
        [3, 1, 5],
        [4, 2, 6],
        [5, 3, 13],
        [6, null, 20]];

    const notNull = value => value !== null;
    const isPossible = index => board[index] !== 0;

    for (let i = 0; i < startIndexes.length; i++) {
        // offset index for 3rd row (14, 15, 16, 17, 18, 19, 20)
        let index = i + 14;
        if (isPossible(index)) {
            const startIndex = startIndexes[i]
            if (_checkVertical(startIndex[0], board)) {
                return true;
            }
            if (notNull(startIndex[1])) {
                if (_checkBackDiagonal(startIndex[1], board)) {
                    return true;
                }
            }
            if (notNull(startIndex[2])) {
                if (_checkForwardDiagonal(startIndex[2], board)) {
                    return true;
                }
            }
        }
    }
    return false;
}

function _checkHorizontal(startIndex, board) {
    const isFourInARow = _trackCount();
    for (let n = startIndex; n < startIndex + 7; n++) {
        if (isFourInARow(board[n])) {
            return true;
        }
    }
    return false;
}
function _checkVertical(startIndex, board) {
    const isFourInARow = _trackCount();
    for (let n = startIndex; n < 42; n += 7) {
        if (isFourInARow(board[n])) {
            return true;
        }
    }
    return false;
}

// \
function _checkBackDiagonal(startIndex, board) {
    const isFourInARow = _trackCount();
    let max = startIndex === 3 ? 30 : 42;

    for (let n = startIndex; n < max; n += 8) {
        if (isFourInARow(board[n])) {
            return true;
        }
    }
    return false;
}

// /
function _checkForwardDiagonal(startIndex, board) {
    const isFourInARow = _trackCount();
    let max = Math.min(startIndex * 7 + 1, 42)
    
    for (let n = startIndex; n < max; n += 6) {
        if (isFourInARow(board[n])) {
            return true;
        }
    }
    return false;
}

function _trackCount() {
    let currentValue = -1;
    let count = 0;
    function isFourInARow(value) {
        if (value === currentValue && value !== 0) {
            count++;
            if (count === 3) { return true };
        }
        else {
            count = 0;
        }
        currentValue = value;
        return false;
    }
    return isFourInARow;
}

export {
    isWinner
}