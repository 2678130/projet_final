
/**
 * Function to hold LookupTables, as we play ai improves its own play
 */
export function LookupTable() {
    let table = {};
    let randomTable = {};

    const getTable = () => {
        return table;
    }

    const updateTable = (board, state) => {
        table[board] = state
    }

    const getRandomTable = (board) => {
        let distribution = randomTable[board];
        console.log({distribution})
        return distribution
    }

    const updateRandomTable = (board, distribution) => {
        if(randomTable[board] === undefined){
            randomTable[board] = new Array(7).fill(0)
        }

        let tempTable = Array.from(randomTable[board])
        randomTable[board] = tempTable.map((value, index) => value + distribution[index])
    }

    return [
        getTable,
        updateTable,
        getRandomTable,
        updateRandomTable
    ]
}