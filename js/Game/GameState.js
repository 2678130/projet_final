/**
 * Projet final de programmation
 * Thierry Bergeron
 * Avril 2020
 * Connect Four
 */

import { AIPlay2 } from "./AI.js"
import { play } from "./BoardServices.js"

/**
 * Function that holds the state of the game
 */
export function GameState() {
    let moveCount = 0;
    let currentBoard = new Array(42).fill(0);
    let winner = undefined;
    let currentPlayerMove = 0;

    // toggle playing modes, 0 -> computer, 1 -> human vs human
    let currentMode = 0;


    /* Game Flow */
    const getBoard = () => {
        return currentBoard;
    }

    /* Human play */
    const playerMove = (playerMove) => {
        // toggle player
        let player = moveCount % 2 === 0 ? 1 : 2;

        // check for draw
        if (moveCount === 41) {
            return "Draw"
        }
        // check for win
        if (winner !== undefined) {
            return winner
        }

        currentPlayerMove = playerMove;
        let outcome = play(currentBoard, playerMove, player);
        // *** perform game updates ***
        // board
        currentBoard = outcome.board
        // increase count
        moveCount++
        // check winner status
        _updateWinner(outcome.message)
    }

    /* Computer play */
    const computerMove = () => {
        // check for win
        if (winner !== undefined) {
            return winner
        }

        // toggle player
        let player = moveCount % 2 === 0 ? 1 : 2;

        // *** Perform play ***
        let actualComputerMove = AIPlay2(currentBoard, player, moveCount);
        let outcome = play(currentBoard, actualComputerMove, player);

        // *** perform game updates ***
        // board
        currentBoard = outcome.board
        // increase count
        moveCount++
        // check winner status
        _updateWinner(outcome.message)
    }

    const getState = () => {
        return winner;
    }

    /* Reset game state */
    const resetGame = () => {
        moveCount = 0;
        winner = undefined;
        currentBoard = new Array(42).fill(0);
    }

    const _updateWinner = (msg) => {
        if (msg > 0) {
            winner = msg;
        }
    }

    const getMode = () => {
        return currentMode;
    }

    const setMode = (mode) => {
        if (currentMode !== mode) {
            currentMode = mode;
            console.log(`set mode invoked`)
            resetGame()
        }
    }

    return [getBoard, playerMove, computerMove, getState, resetGame, getMode, setMode]
}