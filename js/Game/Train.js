import { AIPlay } from "./AI.js"
import { play, getWinningMoves, moveAdded } from "./BoardServices.js"
import { LookupTable } from "./LookupTable.js";

const [getTable, updateTable, getRandomTable, updateRandomTable] = LookupTable();

function bruteForceRandom(board, moveCount, iteration) {
    let distribution = new Array(7).fill(0)
    let startTime = Date.now();
    let executionTime;
    for (let i = 0; i < iteration; i++) {
        let _board = board
        let msg = 0;
        let initialPlayerMove = 0;
        let initialMoveCount = moveCount;
        let currentMoveCount = moveCount;
        let outcome = {};
        while (msg === 0) {
            // determine which player turn it is (1 or 2)
            let player = currentMoveCount % 2 + 1;

            //AIPlay returns the move to play given the current board
            let playerMove = AIPlay(_board, player);

            // play returns an outcome object containing 2 values
            // msg : "1", "2", or "0"
            // board : which is the current board
            outcome = play(_board, playerMove, player);

            // update function variables
            msg = outcome.message;
            _board = outcome.board;


            if (initialMoveCount === currentMoveCount) {
                initialPlayerMove = playerMove;
            }
            currentMoveCount++;
            if (currentMoveCount === 41) {
                msg = "null";
            }
        }
        if (msg === 2) {
            distribution[initialPlayerMove] += 1;
        }
    }
    executionTime = (Date.now()) - startTime;
    console.log({ executionTime });
    updateRandomTable(board, distribution)

    return getRandomTable(board);
}

function playAi(board, moveCount) {
    let startTime = Date.now()
    let outcome;

    
    outcome = miniMaxRecursion(board, moveCount, 5);
    
    let executionTime = Date.now() - startTime;
    let max = Math.max(...outcome.subNodeValues);
    let bestMove = outcome.subNodeValues.map((val, index) =>{
        if(val == max){
            return index;
        }
    }).filter((val => val !== undefined))

    outcome.bestMove = bestMove;
    outcome.max = max;

    console.log({executionTime});
    console.log({bestMove});
    return outcome;
}

/**
 * Recursive minimaxFunction
 */
function miniMaxRecursion(board, moveCount, depth) {
    /* ------------ SEARCH TABLE ------------- */
    if (getTable()[board] !== undefined) {
        return getTable()[board]
    }
    /* --------------------------------------- */

    let subNodeValues = new Array(7).fill(0);
    let currentPlayer = (moveCount % 2) + 1;

    let nodeInfo = {
        winningPlayer: 0,
        depth: depth,
        terminal: false,
        subNodeValues: subNodeValues,
        nodeValue: 0,
    };

    depth--;
    moveCount++;

    /* ----------- TERMINAL NODE ----------- */
    // current board state leads to a winning player
    let winningMoves = getWinningMoves(board, currentPlayer)
    // there's a winner
    if (winningMoves.length > 0) {
        nodeInfo.terminal = true;
        nodeInfo.nodeValue = 10
        for (let i = 0; i < winningMoves.length; i++) {
            subNodeValues[winningMoves[i]] = 10;
        }
        return nodeInfo;
    }

    // maximum recursion depth is reached with no terminal node
    if (depth === 0) {
        return nodeInfo;
    }
    /* --------------------------------------- */

    /* ----------- EVALUATE SUB NODES ----------- */
    // start recursion for every sub nodes
    for (let col = 0; col < 7; col++) {
        // create a board copy
        let _board = Array.from(board);
        if (moveAdded(_board, col, currentPlayer)) {
            let outcome = miniMaxRecursion(_board, moveCount, depth)
            // invert value of subNode
            let value = Math.abs(outcome.nodeValue) > 0 ? outcome.nodeValue * -1 : 0;
            subNodeValues[col] = (value)
        }

        else subNodeValues[col] = - 10;
    }
    /* ------------------------------------------- */

    /* ----------- UPDATE NODE VALUE ----------- */
    let maxNodeValue = Math.max(...subNodeValues);

    // if maxNodeValue is ZERO, we want to have an aggregate

    nodeInfo.nodeValue = maxNodeValue;
    /* ----------------------------------------- */


    /* ----------- UPDATE TABLE ----------- */
    if (Math.abs(nodeInfo.nodeValue) > 0) {
        nodeInfo.terminal = true
    }
    updateTable(board, nodeInfo);

    /* ------------------------------------ */

    return nodeInfo;
}

export {
    bruteForceRandom,
    playAi,
}
