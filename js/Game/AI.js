/**
 * Projet final de programmation
 * Thierry Bergeron
 * Avril 2020
 * Connect Four
 */

import { getWinningMoves, getLosingMoves, getValidMoves, getFutureWinningMove, getFutureLosingMoves } from "./BoardServices.js";
import { bruteForceRandom, playAi } from "./Train.js";

/**
 * 
 * @param {*} board 
 * @param {*} player 
 */
function AIPlay(board, player) {
    // check if theres an immediate winning move
    let immediateWinningMove = _findImmediateWinningMove(board, player);
    if (immediateWinningMove) {
        let num = _getRandomIndexFromArrayLength(immediateWinningMove.length)
        return immediateWinningMove[num];
    }

    // check if we can avoid an immediate lost
    // check if a move leads to a lost, if there's more then one move, take a random move
    let immediateLostAvoidedMove = _avoidImmediateLost(board, player);
    if (immediateLostAvoidedMove) {
        let num = _getRandomIndexFromArrayLength(immediateLostAvoidedMove.length)
        return immediateLostAvoidedMove[num]
    }


    // find a future win
    let futureWinningMove = _findFutureWinningMove(board, player);
    if (futureWinningMove) {
        let num = _getRandomIndexFromArrayLength(futureWinningMove.length)
        return futureWinningMove[num];
    }

    // avoid a future lost
    let futureLostAvoidedMove = _avoidFutureLost(board, player);
    if (futureLostAvoidedMove) {
        let num = _getRandomIndexFromArrayLength(futureLostAvoidedMove.length)
        return futureLostAvoidedMove[num]
    }

    // choose a validMove @ random
    let validMoves = getValidMoves(board, player);
    let randomMove = Math.floor(Math.random() * validMoves.length)

    return validMoves[randomMove]
}

/**
 * 
 * @param {*} board 
 * @param {*} player 
 * @param {*} moveCount 
 */
function AIPlay2(board, player, moveCount) {
    // returns array of valid moves
    let validMoves = getValidMoves(board, player);
    // returns best move from recursion check
    let outcome = playAi(board, moveCount)
    // returns value count from Monte Carlo experiment
    let distribution = bruteForceRandom(board, moveCount, 300)
    // returns best move from Monte Carlo experiment
    let index = distribution.indexOf(Math.max(...distribution))

    if (outcome.bestMove.includes(index) && validMoves.includes(index)) {
        return index
    }

    // choose a validMove @ random
    let randomMove = _getRandomIndexFromArrayLength(validMoves.length)
    return validMoves[randomMove]
}

function _getRandomIndexFromArrayLength(length) {
    return Math.floor(Math.random() * length)
}

function _findFutureWinningMove(board, player) {
    let winningMoves = getFutureWinningMove(board, player)
    if (winningMoves.length > 0) {
        return winningMoves
    }
    return false
}


function _avoidFutureLost(board, player) {
    let avoidFutureLosingMoves = getFutureLosingMoves(board, player)
    if (avoidFutureLosingMoves.length > 0) {
        return avoidFutureLosingMoves
    }
    return false
}


function _findImmediateWinningMove(board, player) {
    let winningMoves = getWinningMoves(board, player)
    if (winningMoves.length > 0) {
        return winningMoves
    }
    return false
}

/**
 * return an array of winning moves for other player, if there's more then one losing move we automatically lose
 * @param {1D array} board 
 * @param {number} player 
 */
function _avoidImmediateLost(board, player) {
    let avoidLosingMoves = getLosingMoves(board, player)
    if (avoidLosingMoves.length > 0) {
        return avoidLosingMoves
    }
    return false
}


export {
    AIPlay,
    AIPlay2,
}