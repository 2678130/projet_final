

import {isWinner} from "./isWinner.js";
import AddMoves from "./addMoves.js";

function play(board, move, player) {
    let _board = _cloneBoard(board);
    // check if move is valid
    if (!_addMove(_board, move, player)) {
        return {
            board: _board,
            message: -1
        }
    }
    // check if move wins
    if (isWinner(_board)) {
        return {
            board: _board,
            message: player
        }
    }
    // update the state
    return {
        board: _board,
        message: 0
    };
}

/**
 * Checks every valid moves and sees if one gives an immediate win
 */
const getWinningMoves = (board, player) => {
    let _winningMoves = new Array(7).fill(false);
    return _winningMoves.map((_, index) => {
        let _board = _cloneBoard(board);

        // Move is not valid, row is full
        if (!_addMove(_board, index, player)) {
            return false;
        }
        // Check if winner
        if (isWinner(_board)) {
            return index;
        }
        // else return false
        return false;
    }).filter(value => value !== false)
}

/**
 * returns an array of winning moves from our opponent
 * @param {1D array} board 
 * @param {number} player 
 */
function getLosingMoves(board, player) {
    let player2 = player === 1 ? 2 : 1;
    return getWinningMoves(board, player2)
}

/**
 * returns an array of possible moves
 * @param {1D array} board 
 * @param {number} player 
 */
function getValidMoves(board, player) {
    let _validMoves = new Array(7).fill(false);
    return _validMoves.map((_, index) => {
        let _board = _cloneBoard(board);
        if (_addMove(_board, index, player)) {
            return index;
        }
        return false
    }).filter(value => value !== false)
}


/**
 * Check if there is a possibility of getting in a situation where a win is inevitable
 */
function getFutureWinningMove(board, player) {
    let _validMoves = new Array(7).fill(false);
    return _validMoves.map((_, index) => {
        let _board = _cloneBoard(board);
        // move is not valid
        if (!_addMove(_board, index, player)) {
            return false;
        }
        // if there is more then one winning move then we have an automatic win
        // TODO: doesnt account for if one blocking move leads to a winning move
        if (getWinningMoves(_board, player).length > 1) {
            return index;
        }
        return false;
    }).filter(value => value !== false)
}


function getFutureLosingMoves(board) {
    return getFutureWinningMove(board, 1)
}



function printBoard(board) {
    let string = ""
    board.forEach((value, index) => {
        //add new line
        if (index > 0 && index % 7 === 0) { string += "\n" }

        string += value + " ";
    })
    console.log(string)
}

function _cloneBoard(board) {
    return Array.from(board)
}

function _addMove(board, move, player) {
    return AddMoves.addMove(board, move, player)
}

/**
 * Adds a given move to the board, places it in the correct row, returns true or false if move is valid
 * 
 * Board passed by reference, FUNCTION CHANGES THE BOARD !!!
 * 
 * @param {1D array} board 
 */
function moveAdded(board, row, player){
    // return no move was added, supplied move is not valid
    if(_moveNotValid(board, row)){
        return false
    }
    // find correct row
    let correctRow = _whichRow(board, row);

    // add/play the move
    board[correctRow] = player;
    return true;
}

/* addMove Helper functions */
function _moveNotValid(board, row){
    // move is valid if top row is "empty", returns false
    if(board[row] === 0){
        return false
    }
    return true
}

function _whichRow(board, row){
    let offset = 6 - row;
        for (let i = 0; i < 6; i++) {
            let currentIndexChecked = 41 - ((i * 7) + offset)
            if (board[currentIndexChecked] === 0) {
                return currentIndexChecked;
            }
        }
}

export {
    getLosingMoves,
    getValidMoves,
    getWinningMoves,
    getFutureWinningMove,
    getFutureLosingMoves,
    play,
    moveAdded,
    printBoard,
}