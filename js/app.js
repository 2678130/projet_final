import { useState, render, createElement, loadcss } from "./BADREACT/BadReact.js";
import { Header } from './components/header.js';
import { Main } from './components/main.js';
import { Nav } from "./components/Nav.js";
import { printBoard } from "./Game/BoardServices.js"
import { GameState } from "./Game/GameState.js"


const root = document.getElementById('root')

// Actual game state
const [getBoard, playMove, computerPlay, getState, resetGame, getMode, setMode] = GameState()
const [instance, updateInstance] = useState(null);

const gameFunctions = {
    getBoard,
    playMove,
    computerPlay,
    resetGame,
    getState,
    getMode,
    setMode
}

const App = () => {
    const time = new Date().toLocaleTimeString();
    const props = {
        root,
        time,
        rerender,
        gameFunctions,
        printBoard,
    }
    return createElement(
        "div",
        { className: "app" },
        Nav(props),
        Header(),
        Main(props),
    )
}

const rerender = () => {
    render(App(), root, instance(), updateInstance)
}

render(App(), root, instance(), updateInstance)
//loadcss()


//setInterval(rerender, 1000)