/**
 * Projet final de programmation
 * Thierry Bergeron
 * Avril 2020
 * Connect Four
 */

/**
 * Copy of react hooks useState
 * @param {Initial Value} initialInstance 
 */
export const useState = (initialInstance) => {
    let instance = initialInstance;
    const state = () => {
        return instance;
    }
    const setState = (currentInstance) => {
        instance = currentInstance;
    }
    return [state, setState]
}