/**
 * Projet final de programmation
 * Thierry Bergeron
 * Avril 2020
 * Connect Four
 */

/**
 * Create a html template element to be rendered
 * @param {string} type 
 * @param {object} props 
 * @param  {...any} children 
 */
export const createElement = (type, props, ...children) => {
    // function invoked if passed props is not an object
    const createTextElement = (text) => {
        return{
            type: "TEXT_ELEMENT",
            props: {
                nodeValue: text,
                children: [],
            }
        }
    }
    return{
        type,
        props: {
            ...props,
            // if child is not an object then create a TextElement
            children: children.map(child => typeof child === "object" ? child : createTextElement(child))
        }
    }
}