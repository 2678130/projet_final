/**
 * Projet final de programmation
 * Thierry Bergeron
 * Avril 2020
 * Connect Four
 */

import {render} from "./render.js"
import {createElement} from "./elements.js"
import {loadcss} from "./loadcss.js"
import {useState} from "./useState.js"

/**
 * render -> 
 *      - updates and renders the dom
 *      - creates, compares and updates the virtual dom
 * 
 * element -> creates an element to the dom
 * useState -> helper function to hold states through closure
 * 
 */

export {
    createElement,
    render,
    useState,
    loadcss
}