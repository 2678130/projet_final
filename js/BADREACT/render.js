/**
 * Projet final de programmation
 * Thierry Bergeron
 * Avril 2020
 * Connect Four
 */

/**
 * Strategy of Badreact / React
 * 
 * Step one - Our javascript code creates an initial tree to render to the dom, in this connect four game
 * that tree is everything in the components folder
 * 
 * Step two - We then want to create / append that tree to a parentDOM
 * to do so we call a recursive function to create and append every element
 * 
 * Obviously if on every small change to our state we destroy and recreate the entire DOM tree, it's not a
 * super great solution and that is where the Virtual Dom comes in.
 * 
 * Step three - When we render our DOM for the first time we also render a matching VDOM. On the following
 * updates, we will compare every element and if they changed, we will update only that part of the DOM and
 * VDOM.
 * 
 * 
 * 
 * @param {element} is the tree to be rendered
 * @param {parentDOM} is the base Dom element
 * @param {rootInstance} -> returns the value of a function that fetch the lates state of the vdom tree
 * @param {updateInstanceRef} -> function to update the state to the current vdom tree
 */
export function render(element, parentDom, rootInstance, updateInstanceRef) {
    // update the current DOM from previous instance
    const nextInstance = reconcile(parentDom, rootInstance, element);

    // update the VDOM to the current DOM for the next compare
    updateInstanceRef(nextInstance)
}


function instantiate(element) {
    // element type detection ex. (h1 vs "some text")
    const { type, props } = element;

    // filter for TEXT_ELEMENT in element creation
    const isTextElement = type === "TEXT_ELEMENT";

    // create dom object depending type
    const dom = isTextElement ? document.createTextNode("") : document.createElement(type);

    // set dom properties
    updateDomProperties(dom, [], props)

    // child element
    const childElements = props.children || [];
    const childInstances = childElements.map(child => instantiate(child))
    const childDoms = childInstances.map(child => child.dom)

    // append every child to the dom
    childDoms.forEach(child => dom.appendChild(child))

    const instance = { dom, element, childInstances }
    return instance
}

/**
 * Function to compare the current output of the tree to the old output
 * 
 * @param {DOM} parentDom 
 * @param {VDOM} instance 
 * @param {CurrentElement from BadReact createElement} element 
 */
function reconcile(parentDom, instance, element) {
    //if the instance is null, create the instance from the element
    if (instance == null) {
        const newInstance = instantiate(element)
        parentDom.appendChild(newInstance.dom)
        return newInstance
    }
    //if the instance is the same as element, update the instance
    if (instance.element.type == element.type) {
        //update current level properties
        updateDomProperties(parentDom, instance.element.props, element.props)

        // check if element.props.children has the same lenght array as childrenInstances, take the longest of the two to iterate
        const numberOfChildren = Math.max(instance.childInstances.length, element.props.children.length)

        // loop for every child
        for (let i = 0; i < numberOfChildren; i++) {
            let childInstance = instance.childInstances[i] || null
            let childElement = element.props.children[i] || null
            reconcileChildren(instance.dom, childInstance, childElement)
        }
        return instance
    }
    //if the instance is different from the element, replace the instance
    else {
        const newInstance = instantiate(element)
        parentDom.replaceChild(newInstance.dom, instance.dom)
        return newInstance
    }
}

// TODO: remove children if they need to be removed
/**
 * Function to reconcile children, calls recursively
 * @param {DOM} parentDom 
 * @param {VDOM} instance 
 * @param {CurrentElement from BadReact createElement} element 
 */
function reconcileChildren(parentDom, instance, element) {
    if (instance == null) {
        const newInstance = instantiate(element)
        parentDom.appendChild(newInstance.dom)
        return newInstance
    }
    if (instance.element.type == element.type) {
        updateDomProperties(instance.dom, instance.element.props, element.props)
        const numberOfChildren = Math.max(instance.childInstances.length, element.props.children.length)

        // loop for every child
        for (let i = 0; i < numberOfChildren; i++) {
            let childInstance = instance.childInstances[i] || null
            let childElement = element.props.children[i] || null
            reconcileChildren(instance.dom, childInstance, childElement)
        }
    }
}


function updateDomProperties(dom, prevProps, currentProps) {

    const isEvent = (key) => key.startsWith("on")
    const isAttribute = (key) => key !== "children" && !isEvent(key)
    const needReplaceAttribute = (key) => prevProps[key] !== currentProps[key]
    //const checkNull = (key) => dom[key] !== undefined
    const needReplaceListener = (key) => hasPrevProps(key) ? prevProps[key].name !== currentProps[key].name : true
    const hasPrevProps = (key) => prevProps[key] !== undefined

    //remove properties
    //remove eventListener
    Object.keys(prevProps)
        .filter(isEvent)
        .filter(hasPrevProps)
        .filter(needReplaceListener)
        .forEach(event => {
            const eventType = event.substring(2)
            dom.removeEventListener(eventType, currentProps[event])
        })

    //remove attribute
    Object.keys(prevProps)
        .filter(isAttribute)
        .filter(needReplaceAttribute)
        .forEach(attribute => {

        })

    //add properties
    //add eventlistener
    Object.keys(currentProps)
        .filter(isEvent)
        .filter(needReplaceListener)
        .forEach(event => {
            const eventType = event.substring(2)
            dom.addEventListener(eventType, currentProps[event])

        })
    //add attribute
    Object.keys(currentProps)
        .filter(isAttribute)
        .forEach(attribute => {
            dom[attribute] = currentProps[attribute]
        })
}