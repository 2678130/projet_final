import { createElement } from "../BADREACT/BadReact.js";
import { NavbarUl } from "./sub_components/NavbarUl.js";


export const Nav = (props) => {
    
    
    return createElement(
        "div",
        { className: "navbar" },
        NavbarUl(props)
    )
}