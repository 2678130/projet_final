import { createElement } from "../BADREACT/BadReact.js";
import { ArrowButton } from "./sub_components/ArrowButton.js";
import { useTaskHandler } from "../Utils/TaskHandler.js";


export const Controls = (props) => {
    const [enqueueTask] = useTaskHandler();
    const gameFunctions = props.gameFunctions;
    const buttonArray = new Array(7).fill(null);

    function handleClick(e) {
        let row = parseInt(e.target.id.substring(3))
        console.log({row})
        gameFunctions.playMove(row)
        props.rerender()
        if(gameFunctions.getMode() === 0){
            enqueueTask(computerPlay)    
        }
    }

    function computerPlay() {
        gameFunctions.computerPlay();
        props.rerender()
    }

    // Button controls
    const buttons = buttonArray.map((_, index) => {
        const buttonProps = {
            cb: handleClick,
            id: "btn" + index,
            className: "control-button",
        }
        return ArrowButton(buttonProps);
    })

    return createElement(
        "div",
        { className: "controls" },
        ...buttons,
    )
}