import { createElement } from "../BADREACT/BadReact.js";
import { Board } from "./Board.js";
import { Controls } from "./Controls.js";
import { WinnerBanner } from "./sub_components/WinnerBanner.js";
import { Div } from "./sub_components/base_components.js";


export const BoardContainer = (props) => {
    const gameFunctions = props.gameFunctions;
    
    //Change board class when game is over
    function updateBoardClass(){
        if(gameFunctions.getState() > 1){
            return "board-container final"
        }
        return "board-container"
    }
    const divProps = {
        className : "board-base"
    }

    return createElement(
        "div",
        { className: updateBoardClass() },
        Controls(props),
        Board(props),
        Div(divProps),
        WinnerBanner(props)
    )
}

