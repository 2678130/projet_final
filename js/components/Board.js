import { createElement } from "../BADREACT/BadReact.js";
import { GameSquare } from "./sub_components/GameSquare.js";



export const Board = (props) => {
    const gameFunctions = props.gameFunctions;

    // Game board
    const board = gameFunctions.getBoard().map(value => {
        const squareProps = {
            value: value
        }
        return GameSquare(squareProps)
    })

    return createElement(
        "div",
        { className: "board" },
        ...board,
    )
}