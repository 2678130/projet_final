import { createElement } from "../../BADREACT/BadReact.js";

const H1 = (props) => {
    let text = props.text
    let className = props.className
    return createElement(
        "h1",
        { className: className },
        text
    )
}

const Button = (props) => {
    const cbFn = props.cb
    const text = props.text
    const className = props.className
    const id = props.id

    return createElement(
        "button",
        { className: className, onclick: cbFn, id: id },
        text
    )
}

const Div = (props) => {
    const text = props.text
    const className = props.className
    const id = props.id
    return createElement(
        "div",
        { className: className, id: id },
        text
    )
}

const Span = (props) => {
    const text = props.text
    const className = props.className
    return createElement(
        "span",
        { className: className },
        text
    )
}

export {
    H1,
    Button,
    Div,
    Span,
}