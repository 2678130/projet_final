import { createElement } from "../../BADREACT/elements.js"
import { Div } from "./base_components.js"

export const NavbarUl = (props) => {

    const handleReset = () => {
        console.log("invoked")
        props.gameFunctions.resetGame()
        props.rerender()
    }
    const getMode = () => {
        return props.gameFunctions.getMode();
    }

    const propsReset = {
        img: "reset-img",
        classNameText: "li-text",
        text: "Reset",
        cb: handleReset,
    }
    const propsHuman = {
        img: "person-img",
        classNameText: "li-text",
        text: "1 vs 1",
        cb: human,
        mode: getMode(),
    }
    const propsComputer = {
        img: "computer-img",
        classNameText: "li-text",
        text: "Ordi vs 1",
        cb: computer,
        mode: getMode(),
    }

    function human() {
        props.gameFunctions.setMode(1);
        props.rerender();
    }

    

    function computer() {
        props.gameFunctions.setMode(0);
        props.rerender();
    }

    const Li = (props) => {
        let className = "li";

        if (props.mode === 1 && props.text === "1 vs 1") {
            className = "li active";
        }
        if (props.mode === 0 && props.text === "Ordi vs 1") {
            className = "li active";
        }

        return createElement(
            "li",
            { className: className, onclick: props.cb },
            Div({ className: props.img }),
            Div({ className: props.classNameText, text: props.text }),
        )
    }

    return createElement(
        "ul",
        { className: "navbar-ul" },
        Li(propsReset),
        Li(propsHuman),
        Li(propsComputer)
    )
}