import {createElement} from "../../BADREACT/BadReact.js";
import { Span } from "./base_components.js";

export const ArrowButton = (props) => {
    const cbFn = props.cb
    const className = props.className
    const id = props.id

    return createElement(
        "button",
        { className: className, onclick: cbFn, id: id },
        Span({className:"arrow one"}),
        Span({className:"arrow two"}),
        Span({className:"arrow three"})
    )
}