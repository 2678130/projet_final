import { createElement } from "../../BADREACT/elements.js"
import { Div } from "./base_components.js"

export const GameSquare = (props) => {
    const divProps = {}

    const squareType = () => {
        let value = props.value
        let className;

        if (value === 1) {
            className = "game-square player-1";
            divProps["className"] = "inset-1"
        }
        else if (value === 2) {
            className = "game-square player-2";
            divProps["className"] = "inset-2"
        }
        else {
            className = "game-square empty";
        }
        return className
    }

    return createElement(
        "div",
        { className: squareType() },
        Div(divProps),
    )
}