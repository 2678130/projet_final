import { createElement } from "../../BADREACT/elements.js"

export const WinnerBanner = (props) => {
    const gameFunctions = props.gameFunctions

    function updateClass(){
        let winner = gameFunctions.getState()
        if(winner === 1){
            return "winner-banner visible-yellow"
        }
        if(winner === 2){
            return "winner-banner visible-red"
        }
        return "winner-banner invisible"
    }
    function updateText(){
        let winner = gameFunctions.getState()
        if(winner === 1){
            return "Le joueur jaune gagne la partie"
        }
        if(winner === 2){
            return "Le joueur rouge gagne la partie"
        }
        return ""
    }

    

    return createElement(
        "div",
        {className: updateClass()},
        updateText()
    )
}