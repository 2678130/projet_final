import { createElement } from "../BADREACT/BadReact.js"
import { BoardContainer } from "./BoardContainer.js"

export const Main = (props) => {
    
    return createElement(
        "div",
        { className: "main" },
        BoardContainer(props),
    )
}