import { createElement } from "../BADREACT/BadReact.js";
import { H1, Div } from "./sub_components/base_components.js"

export const Header = () => {
    return createElement(
        "div",
        { className: "header" },
        Div({ className: "spacer" }),
        H1({
            className: "title",
            text: "Connect 4"
        })
    )
}
