/**
 * Projet final de programmation
 * Thierry Bergeron
 * Avril 2020
 * Connect Four
 */

/**
 * Function used to put tasks in a requestidlecallback queue
 * Code taken from MDN (https://developer.mozilla.org/en-US/docs/Web/API/Background_Tasks_API)
 */
function useTaskHandler(){
    let taskList = [];
    let taskHandle = null;

    function enqueueTasks(taskHandler, taskData) {
        taskList.push({
            handler: taskHandler,
            data: taskData
        });
    
        if (!taskHandle) {
            taskHandle = requestIdleCallback(_runTaskQueue, {timeout : 500})
        }
    }
    
    function _runTaskQueue(deadline) {

        // execute task for deadline (50ms)
        while (deadline.timeRemaining() > 0 || deadline.didTimeout && taskList.length) {
            // Javascript array shift() is an inbuilt function that removes the 
            // first item from an array and returns that removed item.
            let task = taskList.shift()

            if(task === undefined){
                taskHandle = 0;
                console.log("task completed")
            }
            
            else{
                task.handler(task.data)
            }
        }
    
    
        // check if there's still tasks to be executed
        if (taskList.length) {
            taskHandle = requestIdleCallback(_runTaskQueue)
        }
        else{            
            taskHandle = 0;
        }
    }

    function emptyTaskQueue(){
        taskList = []
    }
    return [enqueueTasks, emptyTaskQueue]
}

export {
    useTaskHandler,
}