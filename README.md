# Projet final pour le cours de programmation internet.

## Bibliothèque BadReact

### Introduction

Le projet peut est servi sur https://connect-four-projet.web.app/

Pour la construction du projet final, j'ai construit une bibliothèque permettant
d'implémenter des interfaces utilisateurs. Je l'ai appelé BADREACT. Pour y parvenir,
j'ai consulté les ressources suivantes:

-   React - Fiber Architecture https://github.com/acdlite/react-fiber-architecture#what-is-a-fiber
-   React - Basic Theoretical Concepts https://github.com/reactjs/react-basic
-   Design Principles https://reactjs.org/docs/design-principles.html
-   Pombo - Build your own React https://pomb.us/build-your-own-react/

### BadReact est une bibliothèque inspirée de React (https://reactjs.org/)

-   Elle a été créée dans un but éducatif
-   Elle n'est pas facile à utiliser:
    -   La bibliothèque n'utilise pas [Babel](https://babeljs.io/).
    -   Le pseudo language [JSX](https://reactjs.org/docs/introducing-jsx.html) ne fonctionne pas.
    -   La structure du code html est difficile à visualiser et prédire.
    -   La création des éléments n'est pas intuitive.
-   Elle n'est pas optimisée:
    -   L'arbre VDOM(Virtual [DOM](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction)) est revisité et comparé au complet à chaque appel de la fonction render.
    -   Tous les éléments et parties de l'arbre sont comparés.
-   Elle a été relativement rapide à coder
-   Sa construction est simple et la logique est relativement simple à comprendre.

### BadReact, survol rapide
BadReact est composée de 4 fonctions soit element, loadcss, render puis useState. La fonction la plus complexe est render.
- element sert à créer les différents éléments DOM.
- loadcss sert litéralement à charger le css une fois la page construite.
- useState est utilité pour garder les états des différents éléments de la page, elle est littéralement une copie du useState hooks de react.
- render sert à construire et mettre à jour le DOM de la page.

#### La fonction Render
Etape 1 : Le dossier components contient tous les fichiers javascript qui ensemblent créer un arbre des différents éléments à afficher dans le DOM. Ces mêmes fichiers comporte des fonctions permettant de modifier leurs états internes.

Etape 2 : La fonction render permet la création et l'ajout des différents éléments à un DOM parent. Cette étape se fait
de façons récursive en commencant par la racine, ainsi le DOM est créer et afficher à l'écran.

Si on implémente cette solution sans aucune optimisation, on doit détruire et recréer l'arbre à chaque changement de l'état de notre application. Ce n'est pas super :( La solution de React est de créer un second arbre qui représente l'état précédent de notre application.

Etape 3 : La fonction render accompli alors deux tâches. La première est de comparé chaque élément de l'arbre que l'on veut afficher à l'arbre virtuel. Ça seconde tâche est si l'élément n'est pas existant ou a été modifié, mettre à jour l'élément dans le DOM et le DOM virtuel (arbre virtuel).

## CONNECT 4 Jeux

### Règles du jeux de CONNECT FOUR

-   Le jeux se joue à deux, chaque joueur dépose un jetons à tour de rôle.
-   Le jeux se joue sur un espace de 7 colonnes 6 rangées.
-   L'objectif du jeux est de compléter une séquence de quatre soit vertical, horizontal ou diagonal.
-   Le gagnant est le premier qui atteint l'objectif.

### Construction générale

-   L'espace du jeux est construit sur un tableau unidimensionnel de taille 42
-   La représentation peux être faite comme suit:
```
    0  1  2  3  4  5  6
    7  8  9  10 11 12 13
    14 15 16 17 18 19 20
    21 22 23 24 25 26 27
    28 29 30 31 32 33 34
    35 36 37 38 39 40 41
```

### Trouver le gagnant

1. Rangée horizontale, les indexes de départ sont (0, 7, 14, 21, 28, 35)

```javascript
let startIndex = 7
for(let i = startIndex; i < startIndex + 7; i++)
```

2. Rangée verticale, les indexes de départ sont (0, 1, 2, 3, 4, 5, 6)

```javascript
let startIndex = 1
for(let i = startIndex; i < 42; i += 7)
```

3. Rangée diagonale /, les indexes de départ sont (null, 3, 4, 5, 6, 13, 20)

```javascript
let startIndex = 3
let max = Math.min(startIndex * 7 + 1, 42)
for(let i = startIndex; i < max; i += 6)
```

4. Rangée diagonale \\, les indexes de départ sont (14, 7, 0, 1, 2, 3, null)

```javascript
let startIndex = 7
for(let i = startIndex; i < 42; i += 8)
```

5. Optimisation en regardant les rangées centrales, par exemple, si la case 31 est vide, il est impossible d'avoir une séquence de quatre dans les 4 premières rangées donc nous n'avons pas besoin de vérifier.

### AI de base
#### L'ordinateur à la base fait 4 vérifications dans l'ordre suivant
1. Vérification d'une situation gagnante immédiate.
2. Vérification pour empêcher une situation gagnante immédiate à notre adversaire au prochain tour
3. Vérification d'une situation qui entraine une victoire garantie dans deux tour; une situation qui entraine 2 colonnes gagnantes au prochain tour.
4. Vérification pour empêcher une situation de victoire garantie dans 2 tour; une situation qui entraine 2 colonnes gagnantes à notre adversaire à son prochain tour.

#### Pour améliorer la qualité de son jeux
- Si l'ordinateur joue de façon uniquement aléatoire en appliquant les 4 vérifications notées plus haut, la qualité de son jeux n'est pas très bonne.
- La méthode employée pour améliorer la qualité de son jeux est très simple, à partir de l'état actuel du jeux, l'ordinateur joue contre lui-même jusqu'à ce qu'il ait un gagnant. Il répète cette étape nombreuses fois puis prend en note le nombre de victoire dans chaque colonne de départ et joue celle qui a mené au plus grand nombre de victoire.